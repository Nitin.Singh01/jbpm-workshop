package KafkaConsume;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
	@SpringBootApplication

	@RestController
	public class KafkaConsumerApplication {
		
		List<Object> messages = new ArrayList<>();

		

		@GetMapping("/EventConsumer")
		public List<Object> consumeMsg() {
			return messages;
		}



		@KafkaListener(groupId = "consumer-test-group", topics = "EventStream", containerFactory = "kafkaListenerContainerFactory")
		public List<Object> getMsgFromTopic(String data) throws JsonProcessingException {
			ObjectMapper mapper = new ObjectMapper();
			
			Trans dat = mapper.readValue(data, Trans.class);
			messages.add(dat);
			return messages;
		}




		public static void main(String[] args) {
			SpringApplication.run(KafkaConsumerApplication.class, args);
		}
	

	}
