package KafkaConsume;



public class Trans {
	int Transaction_ID;
	String Acc_ID;
	String Transaction_Type;
	float Amount;
	public Trans(int transaction_ID, String acc_ID, String transaction_Type, float amount) {
		super();
		Transaction_ID = transaction_ID;
		Acc_ID = acc_ID;
		Transaction_Type = transaction_Type;
		Amount = amount;
	}

	public Trans() {}
	public int getTransaction_ID() {
		return Transaction_ID;
	}
	public void setTransaction_ID(int transaction_ID) {

		
		this.Transaction_ID = transaction_ID;
	}
	public String getAcc_ID() {
		return Acc_ID;
	}
	public void setAcc_ID(String acc_ID) {
		Acc_ID = acc_ID;
	}
	public String getTransaction_Type() {
		return Transaction_Type;
	}
	public void setTransaction_Type(String transaction_Type) {
		Transaction_Type = transaction_Type;
	}
	public float getAmount() {
		return Amount;
	}
	public void setAmount(float amount) {
		Amount = amount;
	}
	

}
