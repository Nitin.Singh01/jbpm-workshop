package KafkaPublish;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;



@SpringBootApplication
@RestController
public class KafkaPublisherApplication {
	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	private KafkaTemplate<String, Object> template;

	private String topic1 = "javatechie";
	private String topic2 = "EventStream";

	@GetMapping("/publish/{name}")
	public String publishMessage(@PathVariable String name) {
		template.send(topic1, "Hi " + name + " Welcome to java techie");
		return "Data published";
	}

	@RequestMapping(value="/publishJson", method=RequestMethod.POST)
	public String publishMessage(@RequestBody Trans tran) throws JsonProcessingException {
		createEventRecord(tran);
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(tran);
		template.send(topic2, jsonString);
		return "Json Data published";
	}

	public static void main(String[] args) {
		SpringApplication.run(KafkaPublisherApplication.class, args);
	}
	public void createEventRecord(Trans tran ) {jdbcTemplate.execute("INSERT INTO EventsRecord (Acc_Number,Transaction_Type,Amount) values("+tran.getAcc_ID()+","+
			"\""+tran.getTransaction_Type()+"\""+","+tran.getAmount()+");");}
}
